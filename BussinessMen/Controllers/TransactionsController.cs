﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BussinessMen.Models;
using BussinessMen.Tools;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace BussinessMen.Controllers
{
    [Route("api/[controller]")]
    public class TransactionsController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly GetTransactions _api;
        private readonly TransactionDbTools _dbTools;

        public TransactionsController(ApplicationDbContext db, IConfiguration conf)
        {
            _db = db;
            _api = new GetTransactions(conf);
            _dbTools = new TransactionDbTools(db);
        }


        // GET: api/values
        [HttpGet]
        [Route("GetAll")]
        public async Task<List<TransactionViewModel>> GetAll()
        {
            List<TransactionViewModel> model = new List<TransactionViewModel>();
            try
            {
                if (ConnectionTools.CheckForInternetConnection())
                    model = await _api.GetResult();

                if (model.Count > 0)
                {
                    _dbTools.InsertList(model);
                    return model;
                }

                model = await _dbTools.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return model;
        }
    }
}
