﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BussinessMen.Models;
using BussinessMen.Tools;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace BussinessMen.Controllers
{
    [Route("[controller]")]
    public class HomeController : Controller
    {
        private readonly GetTransactions _api;
        private readonly TransactionDbTools _dbTools;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ApplicationDbContext db, IConfiguration conf, ILogger<HomeController> logger)
        {
            _api = new GetTransactions(conf);
            _dbTools = new TransactionDbTools(db);
            _logger = logger;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            _logger.LogInformation("Index view Web");
            return View(new List<TransactionWebViewModel>());
        }

        [HttpPost]
        public async Task<IActionResult> Index(string sku)
        {
            _logger.LogInformation("Consulta de SKU: " + sku);
            return View(await _dbTools.GetListBySku(sku));
        }

    }
}
