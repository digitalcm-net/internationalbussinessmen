﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using BussinessMen.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace BussinessMen.Tools
{
    public class GetTransactions : GetResources<TransactionViewModel>
    {
        protected new IConfiguration _resources;

        public GetTransactions(IConfiguration resources) : base(resources)
        {
            _resources = resources;
        }

        public override async Task<List<TransactionViewModel>> GetResult()
        {
            List<TransactionViewModel> transactionViewModels = new List<TransactionViewModel>();

            try
            {
                HttpClient client = Initial();
                HttpResponseMessage res = await client.GetAsync(_resources.GetValue<string>("Resources:Transactions"));

                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    transactionViewModels = JsonConvert.DeserializeObject<List<TransactionViewModel>>(result);
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return transactionViewModels;
        }
    }
}
