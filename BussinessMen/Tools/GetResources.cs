﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace BussinessMen.Tools
{
    public abstract class GetResources<T>
    {
        protected IConfiguration _resources;

        protected GetResources(IConfiguration resources)
        {
            _resources = resources;
        }

        public HttpClient Initial()
        {
            var Client = new HttpClient();

            string endpoint = _resources.GetValue<string>("Resources:EndPoint");

            Client.BaseAddress = new Uri(endpoint);

            return Client;
        }

        public abstract Task<List<T>> GetResult();

    }
}
