﻿using System;
using System.Net;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace BussinessMen.Tools
{
    public class ConnectionTools
    {
        private static IConfiguration _resources;
        private static ILogger<ConnectionTools> _logger;

        public ConnectionTools(IConfiguration resources, ILogger<ConnectionTools> logger)
        {
            _resources = resources;
            _logger = logger;
        } 

        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead(_resources.GetValue<string>("Resources:EndPoint")))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error en Conexión");
                return false;
            }
        }
    }
}
