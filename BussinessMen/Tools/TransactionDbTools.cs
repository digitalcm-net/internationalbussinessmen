﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BussinessMen.Models;
using Microsoft.EntityFrameworkCore;

namespace BussinessMen.Tools
{
    public class TransactionDbTools : DatabaseTools<TransactionViewModel>
    {
        protected new ApplicationDbContext _db;
        private readonly CalculateRateEUR _rateEUR;

        public TransactionDbTools(ApplicationDbContext db) : base(db)
        {
            _db = db;
            _rateEUR = new CalculateRateEUR(db);
        }

        public async Task<List<TransactionWebViewModel>> GetListBySku(string sku)
        {
            List<TransactionWebViewModel> transactionViewModels = await (from tran in _db.Transactions
                                                                      where tran.SKU == sku
                                                                      select new TransactionWebViewModel()
                                                                      {
                                                                          SKU = tran.SKU,
                                                                          Amount = Math.Round(tran.Amount, 2),
                                                                          Currency = tran.Currency

                                                                      }).AsNoTracking().ToListAsync();
            

            return await _rateEUR.CalculateEUR(transactionViewModels);
        }

        public override void DeleteAll()
        {
            _db.Transactions.Clear();
        }

        public override async Task<List<TransactionViewModel>> GetList()
        {
            List<TransactionViewModel> transactionViewModels = await(from tran in _db.Transactions
                                                                     select new TransactionViewModel()
                                                                     {
                                                                         SKU = tran.SKU,
                                                                         Amount = tran.Amount,
                                                                         Currency = tran.Currency

                                                                     }).AsNoTracking().ToListAsync();

            return transactionViewModels;
        }

        public override void InsertList(List<TransactionViewModel> list)
        {
            try
            {
                List<Transaction> transactionsList = new List<Transaction>();

                foreach (TransactionViewModel row in list)
                {
                    Transaction transaction = new Transaction()
                    {
                        Id = new Guid(),
                        SKU = row.SKU,
                        Amount = Math.Round(row.Amount, 2),
                        Currency = row.Currency
                    };

                    transactionsList.Add(transaction);
                }

                _db.Transactions.AddRange(transactionsList);

                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
