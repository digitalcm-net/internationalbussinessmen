﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BussinessMen.Models;

namespace BussinessMen.Tools
{
    public abstract class DatabaseTools<T>
    {
        protected readonly ApplicationDbContext _db;

        protected DatabaseTools(ApplicationDbContext db)
        {
            _db = db;
        }

        public abstract Task<List<T>> GetList();
        public abstract void InsertList(List<T> list);
        public abstract void DeleteAll();
    }
}
