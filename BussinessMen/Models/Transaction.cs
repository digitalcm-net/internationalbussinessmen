﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BussinessMen.Models
{
    public class Transaction
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(5)")]
        public string SKU { get; set; }
        [Required]
        public double Amount { get; set; }
        [Required]
        [Column(TypeName = "varchar(3)")]
        public string Currency { get; set; }
    }

    public class TransactionViewModel
    {
        public string SKU { get; set; }
        public double Amount { get; set; }
        public string Currency { get; set; }
    }

    public class TransactionWebViewModel : TransactionViewModel
    {
        public double NewAmount { get; set; }
        public string NewCurrency { get; set; }
    }
}
