﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using BussinessMen.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Serilog.Core;

namespace BussinessMen.Tools
{
    public class GetRates : GetResources<RatesViewModel>
    {
        protected new IConfiguration _resources;

        public GetRates(IConfiguration resources) : base(resources)
        {
            _resources = resources;
        }

        public override async Task<List<RatesViewModel>> GetResult()
        {
            List<RatesViewModel> ratesView = new List<RatesViewModel>();

            try
            {
                HttpClient client = Initial();
                HttpResponseMessage res = await client.GetAsync(_resources.GetValue<string>("Resources:Rates"));

                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    ratesView = JsonConvert.DeserializeObject<List<RatesViewModel>>(result);
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return ratesView;
        }

    }
}
