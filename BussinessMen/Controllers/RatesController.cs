﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BussinessMen.Models;
using BussinessMen.Tools;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace BussinessMen.Controllers
{
    [Route("api/[controller]")]
    public class RatesController : Controller
    {

        private readonly ApplicationDbContext _db;
        private readonly GetRates _api;
        private readonly RateDbTools _dbTools;

        public RatesController(ApplicationDbContext db, IConfiguration conf)
        {
            _db = db;
            _api = new GetRates(conf);
            _dbTools = new RateDbTools(db);
        }

        // GET: api/values
        [HttpGet]
        [Route("GetAll")]
        public async Task<List<RatesViewModel>> GetAll()
        {
            List<RatesViewModel> model = new List<RatesViewModel>();

            if(ConnectionTools.CheckForInternetConnection())
                model = await _api.GetResult();

            if (model.Count > 0)
            {
                _dbTools.InsertList(model);
                return model;
            }
                
            model = await _dbTools.GetList();

            return model;
        }
    }
}
