﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BussinessMen.Models;

namespace BussinessMen.Tools
{
    public abstract class CalculateRatesBase
    {
        public CalculateRatesBase()
        {

        }
        public abstract Task<List<TransactionWebViewModel>> CalculateEUR(List<TransactionWebViewModel> transactionViewModels);

    }
}
