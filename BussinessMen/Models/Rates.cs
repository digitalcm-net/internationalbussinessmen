﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BussinessMen.Models
{
    public class Rates
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(3)")]
        public string From { get; set; }
        [Required]
        [Column(TypeName = "varchar(3)")]
        public string To { get; set; }
        [Required]
        public double Rate { get; set; }
    }

    public class RatesViewModel
    {
        public string From { get; set; }
        public string To { get; set; }
        public double Rate { get; set; }
    }
}
