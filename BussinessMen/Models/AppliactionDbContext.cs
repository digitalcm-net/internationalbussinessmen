﻿using System;
using Microsoft.EntityFrameworkCore;

namespace BussinessMen.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<Rates> Rates { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

    }
}
