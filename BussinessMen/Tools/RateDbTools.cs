﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BussinessMen.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace BussinessMen.Tools
{
    public class RateDbTools : DatabaseTools<RatesViewModel>
    {
        protected new ApplicationDbContext _db;

        public RateDbTools(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public override void DeleteAll()
        {
            _db.Rates.Clear();
        }

        public override async Task<List<RatesViewModel>> GetList()
        {
            List<RatesViewModel> ratesViewModels = await (from rate in _db.Rates
                                                 select new RatesViewModel()
                                                 {
                                                     To = rate.To,
                                                     From = rate.From,
                                                     Rate = rate.Rate

                                                 }).AsNoTracking().ToListAsync();
            return ratesViewModels;
        }

        public override void InsertList(List<RatesViewModel> list)
        {
            try
            {
                DeleteAll();

                List<Rates> ratesList = new List<Rates>();

                foreach (RatesViewModel row in list)
                {
                    Rates rates = new Rates()
                    {
                        Id = new Guid(),
                        To = row.To,
                        From = row.From,
                        Rate = row.Rate
                    };

                    ratesList.Add(rates);
                }

                _db.Rates.AddRange(ratesList);

                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

    }
}
