﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using BussinessMen.Models;
using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace BussinessMen.Tools
{
    public class CalculateRateEUR : CalculateRatesBase
    {
        private readonly RateDbTools _rateDbTools;
        private List<RatesViewModel> _ListAux;


        public CalculateRateEUR(ApplicationDbContext db)
        {
            _rateDbTools = new RateDbTools(db);
        }


        public override async Task<List<TransactionWebViewModel>> CalculateEUR(List<TransactionWebViewModel> transactionViewModels)
        {
            List<TransactionWebViewModel> webViewModels = new List<TransactionWebViewModel>();

            foreach (TransactionViewModel row in transactionViewModels)
            {
                TransactionWebViewModel line = new TransactionWebViewModel()
                {
                    Amount = row.Amount,
                    SKU = row.SKU,
                    Currency = row.Currency
                };
                List<RatesViewModel> rates = await _rateDbTools.GetList();

                if (row.Currency == "EUR")
                {
                    line.NewAmount = row.Amount;
                    line.NewCurrency = row.Currency;
                }
                else
                {
                    List<RatesViewModel> ratesList = rates.Where(m => m.To == "EUR").ToList();

                    _ListAux = new List<RatesViewModel>();
                    Cal(rates, row.Currency, "EUR");

                    double auxAmount = row.Amount;
                    double tempAmount = new double(); 

                    for(int i = _ListAux.Count - 1; i >= 0; i--)
                    {
                        RatesViewModel ede = _ListAux.ElementAt(i);
                        tempAmount += Math.Round(auxAmount * ede.Rate);
                        auxAmount = tempAmount;
                    }

                    line.NewAmount = tempAmount;
                    line.NewCurrency = "EUR";
                }

                webViewModels.Add(line);
            }

            return webViewModels;
        }

        private void Cal(List<RatesViewModel> model, string currency, string line)
        {
            // currency => AUD
            //USD EUR 1.43
            //EUR USD 0.7
            //USD AUD 0.52
            //AUD USD 1.92
            //AUD CAD 1.15
            //CAD EUR 0.87

            if (model.Exists(m => m.To == line && m.From == currency))
            {
                _ListAux.Add(model.First(m => m.To == line && m.From == currency));
                return;
            }
                

            foreach(RatesViewModel row in model.Where(x => x.To == line))
            {
                if(row.From == currency)
                {
                    _ListAux.Add(row);
                    break;
                }
                else
                {
                    _ListAux.Add(row);
                    Cal(model, currency, row.From);
                    break;
                }
            }

        }
    }
}
